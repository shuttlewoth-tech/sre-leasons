---
sidebar_position: 2
---

# How to prepare a Windows machine
:::caution
Start by installing Chocolatey as it will be your package manager
:::

## Tools
### ![](https://community.chocolatey.org/favicon.ico) [Chocolatey](https://community.chocolatey.org/courses/getting-started/what-is-chocolatey) 

Chocolatey is a software management solution unlike any you've ever experienced on Windows. You create a software deployment package using a little PowerShell, then you can deploy it anywhere you have Windows with everything (like Puppet, SCCM, Altiris, Connectwise Automate, etc). And the best part is you can take advantage of Chocolatey without any cost (yes, even internally for an organization)!

:::tip
To install **Chocolatey** simply run the code below in a admin powershell window
```shell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```
:::

:::tip
Chocolatey lets you install almost any windows application simply check that its available **[Here](https://community.chocolatey.org/packages?q=)** then using powershell run `choco install <Name of application>`
:::

### [MS Visual Studio Code](https://code.visualstudio.com/)

MS Code is a great free IDE that works on all Operating systems

:::info
An IDE is an Integrated Development Environment is also a computer program where you can edit plain text, but it also has tools and features to assist a software developer do their work. 
Some of the most common features include a text editor (with syntax highlighting), 
code analysing tools providing information, automatic code completion etc., 
build automation tools (e.g no more manual compilation, dependency resolution) and a debugger (which - simplistically - is a tool that allows you to analyse a program that's running).
:::

:::tip
To install **MS Code** simply run the code below in a admin powershell window
```shell
choco install vscode
```
:::
