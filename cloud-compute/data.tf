locals {
  gcp_region = jsondecode(data.local_file.top_gcp_region.content)["gcp_region"]
}

variable "ip_geo_location_api_key" {
  type    = string
  default = "3733a93ae5d84aeaa6c1dce7c5757a43"
}

module "get_ip" {
  source = "./modules/get-ip"
}

module "ip_geo_location" {
  source  = "./modules/ip-geo-location"
  api_key = var.ip_geo_location_api_key
  ip      = module.get_ip.ip
}

data "local_file" "top_gcp_region" {
  filename = "./.gcp_region"
}