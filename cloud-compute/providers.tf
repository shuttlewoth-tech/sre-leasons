provider "google" {
  project = "shuttleworth-core"
  region  = local.gcp_region
}