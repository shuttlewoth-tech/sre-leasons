#!/bin/bash

set -e

gcping_path="/tmp/gcping"

function install_gcping() {
  if ! [ -f ${gcping_path} ]; then
    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      local DISTRIB
      DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
      if [[ ${DISTRIB} = "Ubuntu"* ]]; then
        if uname -a | grep -q '^Linux.*Microsoft'; then
          echo "ubuntu via WSL Windows Subsystem for Linux is not supported"
        else
          # native ubuntu
          curl https://storage.googleapis.com/gcping-release/gcping_linux_amd64_latest >"${gcping_path}" && chmod +x "${gcping_path}"
        fi
      elif [[ ${DISTRIB} = "Debian"* ]]; then
        # debian
        curl https://storage.googleapis.com/gcping-release/gcping_linux_amd64_latest >"${gcping_path}" && chmod +x "${gcping_path}"
      fi
    elif [[ "$OSTYPE" == "darwin"* ]]; then
      # macOS OSX
      curl https://storage.googleapis.com/gcping-release/gcping_darwin_amd64_latest >"${gcping_path}" && chmod +x "${gcping_path}"
    fi
  fi
}

function generate_region_file() {
  region_file="./.gcp_region"
  if ! [ -f ${region_file} ]; then
    install_gcping
    top_gcp_region=$(${gcping_path} -top)
    jq -n --arg gcp_region "${top_gcp_region}" '{"gcp_region":$gcp_region}' >"${region_file}"
  fi
  cat "${region_file}"
}

generate_region_file
