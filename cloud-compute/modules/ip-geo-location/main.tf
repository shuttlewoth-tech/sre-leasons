terraform {
  required_providers {
    http = {
      source  = "hashicorp/http"
      version = "3.0.1"
    }
  }
}

variable "ip" {
  type = string
}

variable "api_key" {
  type = string
}

data "http" "get_geo_location" {
  url = "https://api.ipgeolocation.io/ipgeo?apiKey=${var.api_key}&ip=${var.ip}"

  request_headers = {
    Accept = "application/json"
  }

  lifecycle {
    postcondition {
      condition     = contains([200], self.status_code)
      error_message = "Status code invalid"
    }
  }
}

output "raw" {
  value = data.http.get_geo_location.body
}