locals {
  cloud_config_template = jsondecode(file("${path.module}/resources/cloud-config.json"))
  user_template = {
    shell  = "/usr/bin/zsh"
    sudo   = "ALL=(ALL) NOPASSWD:ALL"
    groups = "docker"
  }
  default_packages = [
    "apt-transport-https",
    "ca-certificates",
    "containerd.io",
    "dirmngr",
    "docker-ce",
    "docker-ce-cli",
    "docker-compose-plugin",
    "gnupg",
    "google-cloud-cli",
    "kr",
    "kubectl",
    "software-properties-common",
    "terraform",
    "zsh",
  ]

  cloud_config = merge({
    write_files = [
      {
        content     = "10 * * * * /usr/local/sbin/idle-stop.sh \n"
        owner       = "root:root"
        permissions = "0777"
        path        = "/etc/cron.d/idle-stop"
      },
      {
        content     = base64encode(file("${path.module}/resources/idle-stop.sh"))
        encoding    = "b64"
        owner       = "root:root"
        permissions = "0777"
        path        = "/usr/local/sbin/idle-stop.sh"
      }
    ]
    users    = [for user in var.users : merge(local.user_template, user)]
    groups   = ["docker"]
    packages = concat(var.additional_packages, local.default_packages)
  }, local.cloud_config_template, var.cloud_config_override)
}
