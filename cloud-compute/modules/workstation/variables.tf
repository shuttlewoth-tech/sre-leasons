variable "gcp_region" {
  type        = string
  description = "The GCP Region the cluster will be created in."
}

variable "prefix" {
  type = string
}

variable "users" {
  type = list(object({
    name                = string
    ssh_authorized_keys = list(string)
  }))
}

variable "additional_packages" {
  type    = list(string)
  default = []
}

variable "cloud_config_override" {
  default = {}
  validation {
    condition     = can(jsonencode(var.cloud_config_override))
    error_message = "cloud_config_override isn't jsonencode compatible."
  }
}

variable "machine_type" {
  type    = string
  default = "e2-micro"
}

variable "gcp_network" {
  type = string
}

variable "gcp_subnetwork" {
  type = string
}

variable "home_disk_size" {
  type    = number
  default = 10
}