locals {
  env_id = "${var.prefix}-workstation-dev"
  users  = [for user in var.users : user["name"]]
}

resource "google_service_account" "main" {
  account_id   = local.env_id
  display_name = local.env_id
}

resource "google_compute_address" "main" {
  name   = local.env_id
  region = var.gcp_region
}

resource "google_compute_instance" "main" {
  name         = local.env_id
  machine_type = var.machine_type
  zone         = data.google_compute_zones.main.names[0]

  allow_stopping_for_update = true
  scheduling {
    automatic_restart  = false
    preemptible        = true
    provisioning_model = "SPOT"
  }


  boot_disk {
    initialize_params {
      image = data.google_compute_image.main.self_link
    }
  }

  attached_disk {
    source      = google_compute_disk.user_home.self_link
    device_name = "home"
  }

  network_interface {
    subnetwork = var.gcp_subnetwork
    access_config {
      nat_ip = google_compute_address.main.address
    }
  }

  metadata = {
    user-data = "#cloud-config\n${yamlencode(local.cloud_config)}"
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.main.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_disk" "user_home" {
  name = "user-home"
  type = "pd-ssd"
  zone = data.google_compute_zones.main.names[0]
  size = var.home_disk_size
}