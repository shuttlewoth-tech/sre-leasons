data "google_compute_zones" "main" {
  region = var.gcp_region
  status = "UP"
}

data "google_compute_image" "main" {
  family  = "ubuntu-2004-lts"
  project = "ubuntu-os-cloud"
}