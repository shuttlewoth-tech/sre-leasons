#!/bin/bash

function check_active_sessions() {
  user_count="$(who -q | tail -n +2)"
  user_count="${user_count#"# users="}"
  if [ "$user_count" -gt 0 ]; then
    echo "${user_count} active users"
      return 1
  fi
  echo "No active users"
  return 0
}

function check_past_sessions() {
  past_sessions="$(last -s "-30min" | head -n -2)"
  if [ -z "$past_sessions" ]; then
    echo "${past_sessions}"
      return 1
  fi
  echo "No past sessions"
  return 0
}


if check_active_sessions; then
  if check_past_sessions; then
      shutdown -h now
  fi
fi

