terraform {
  required_providers {
    http = {
      source  = "hashicorp/http"
      version = "3.0.1"
    }
  }
}

data "http" "get_ip" {
  url = "https://api.ipgeolocation.io/getip"

  #  request_headers = {
  #    Accept = "application/json"
  #  }

  lifecycle {
    postcondition {
      condition     = contains([200], self.status_code)
      error_message = "Status code invalid"
    }
  }
}
locals {
  ip = jsondecode(data.http.get_ip.body)["ip"]
}

output "ip" {
  value = local.ip
}