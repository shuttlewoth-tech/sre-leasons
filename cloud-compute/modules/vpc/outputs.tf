output "gcp_region" {
  value = google_compute_router.vpc_router.region
}

output "public_network" {
  value = google_compute_network.vpc.name
}

output "public_subnetwork" {
  value = google_compute_subnetwork.public.name
}