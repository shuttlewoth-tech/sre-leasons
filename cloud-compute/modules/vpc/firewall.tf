resource "google_compute_firewall" "public_allow_restricted_network_inbound" {
  name = "${local.name_prefix}-allow-restricted-inbound"

  network = google_compute_network.vpc.name

  direction = "INGRESS"

  source_ranges = var.source_range

  priority = "1000"

  allow {
    protocol = "all"
  }
}