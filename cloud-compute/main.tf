terraform {
  backend "http" {
  }
}

locals {
  prefix = random_pet.prefix.id
  users = [
    {
      name                = "teslasolari"
      ssh_authorized_keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC7k5o4VO4FNuXVjAnFqg/0z2+FfX/6F+3Gvmv8dgf1HlpY9BG6rv+h/b5uXR5vC/0e84OaEv/ZnvPsqWOx+HgbwtniEEAlmfRJQB6xPW/33E1oiR6ftybfQxwB+o/ruxAGgb3dgG8v+FCFJI7Jyk9dEOvJ8ybLdBF6y6AYtdRtuqp7oKzj5AHySbH8+8YUzXGPvG/P4GQeiAceTipaOrR8ha76e4Pp/nb08wFX9hTtz/2SFbEy4mDOoacy5Cp1EM98y9wsAZiEEM13MY/5GfahhZoufi9hDaWe+Sb75m5Qu+BqPjcDe0uJ2DzsdjPYkB0l6vY6pc9OoNIX2Q53DBJZnvKKkW73eMaGyEz3N+aUXS43UKcu+ev9cpU6jw0A13Zn6Xt2L/XBbVSxs7fb34LZaUCArbGX4v2a2woSJ67FnrZ/aFWBKyUWMrYZ9o3J+HiuyqVA98bJ4XDv8gRaTf3hUwRANj73tCBzGX7Z7LPKQHDHRJDhz5oQv1HiJcR5Kv0= Pixel6"]
    }
  ]
}

resource "random_pet" "prefix" {}

module "vpc" {
  source       = "./modules/vpc"
  prefix       = local.prefix
  region       = local.gcp_region
  source_range = ["77.248.116.8/32"]
}

module "workstation" {
  source         = "./modules/workstation"
  gcp_region     = module.vpc.gcp_region
  gcp_network    = module.vpc.public_network
  gcp_subnetwork = module.vpc.public_subnetwork
  machine_type   = "e2-medium"
  prefix         = local.prefix
  users          = local.users
}